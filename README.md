# PerDayPvP #
## Features ##
* Specify days that certain worlds on your server will allow player-vs-player fighting
## Commands ##
* None
## Permissions ##
* None
## Installation Process ##
1. Download and uncompress the plugin zip
2. Place the jar file and the PerDayPvP folder into your servers plugin folder
3. Edit the configuration to your liking
## Important Things to Note ##
* IN THE CONFIGURATION, THE FORMAT FOR THE DAYS LIST GOES AS FOLLOWS: MM/DD/YYYY
* THE "null" ENTRIES IN THE DEFAULT CONFIG ARE THERE JUST AS PLACEHOLDERS! PLEASE EDIT THEM AS IF YOU DON'T, THIS PLUGIN WILL DO NOTHING!
## How to edit the configuration file ##
1. Browse to your servers plugin folder
2. Find the folder named "PerDayPvP"
3. Open that folder, then open the "config.yml" file inside of there with a text editor
4. Edit the "Days" and "Worlds" list with any information you would like
# Default Configuration #
```
#!YAML
Days:
- null
Worlds:
- null
```
# Contributors #
* Robert Trainor
# Download PerDayPvP #
[Download From BukkitDev](http://dev.bukkit.org/bukkit-plugins/pvpperday/files/)