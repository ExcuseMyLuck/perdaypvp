package me.eml.pdpvp.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;

public class LogManager {
	public static Logger log = Logger.getLogger("Server");
	public static PluginDescriptionFile pdFile = Bukkit.getServer()
			.getPluginManager().getPlugin("PerDayPvP").getDescription();

	public static void logInfo(String logMessage) {
		log.info(logMessage);
	}

	public static void logWarning(String logMessage) {
		log.log(Level.WARNING, logMessage);
	}

	public static void logSevere(String logMessage) {
		log.log(Level.SEVERE, logMessage);
	}

	public static void logEnable() {
		log.info(pdFile.getName() + " by " + pdFile.getAuthors()
				+ " has been enabled.");
	}

	public static void logDisable() {
		log.info(pdFile.getName() + " by " + pdFile.getAuthors()
				+ " has been disabled.");
	}
}
