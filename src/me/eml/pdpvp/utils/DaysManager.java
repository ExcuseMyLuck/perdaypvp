package me.eml.pdpvp.utils;

import java.util.Calendar;
import java.util.List;

import me.eml.pdpvp.PerDayPvP;

public class DaysManager {
	private PerDayPvP plugin;

	public DaysManager(PerDayPvP plugin) {
		this.plugin = plugin;
	}

	public void checkDate() {
		List<String> pvpDays = plugin.getConfig().getStringList("Days");
		List<String> pvpWorlds = plugin.getConfig().getStringList("Worlds");
		for (String str : pvpDays) {
			String[] days = str.split("/");
			String month = days[0];
			int monthInt = Integer.decode(month) - 1;
			String day = days[1];
			int dayInt = Integer.decode(day);
			String year = days[2];
			int yearInt = Integer.decode(year);

			if (Calendar.getInstance().get(Calendar.YEAR) == yearInt) {
				if (Calendar.getInstance().get(Calendar.MONTH) == monthInt) {
					if (Calendar.DAY_OF_MONTH == dayInt) {
						for (String worlds : pvpWorlds) {
							if (plugin.getServer().getWorld(worlds) == null) {
								LogManager
										.logSevere(PerDayPvP.prefix
												+ "UNABLE TO TURN OFF PVP IN A WORLD THAT DOESN'T EXIST: "
												+ worlds);
							} else {
								plugin.getServer().getWorld(worlds)
										.setPVP(true);
								LogManager
										.logInfo(PerDayPvP.prefix
												+ "PVP ENABLED IN ALL SPECIFIED WORLDS!");
							}
						}
					}
				}
			}

			if ((Calendar.getInstance().get(Calendar.YEAR) != yearInt)
					&& (Calendar.getInstance().get(Calendar.MONTH) != monthInt)
					&& (Calendar.DAY_OF_MONTH != dayInt)) {
				for (String worlds : pvpWorlds) {
					if (plugin.getServer().getWorld(worlds) == null) {
						LogManager
								.logSevere(PerDayPvP.prefix
										+ "UNABLE TO TURN OFF PVP IN A WORLD THAT DOESN'T EXIST: "
										+ worlds);
					} else {
						plugin.getServer().getWorld(worlds).setPVP(false);
						LogManager.logInfo(PerDayPvP.prefix
								+ "PVP DISABLED IN ALL SPECIFIED WORLDS!");
					}
				}
			}
		}
	}
}
