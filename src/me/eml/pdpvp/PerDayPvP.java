package me.eml.pdpvp;

import java.io.File;
import java.util.List;

import me.eml.pdpvp.utils.DaysManager;
import me.eml.pdpvp.utils.LogManager;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class PerDayPvP extends JavaPlugin {
	public PluginDescriptionFile pdFile = getDescription();
	private static File configFile = new File("plugins/PerDayPvP/config.yml");
	public static String prefix = "[AVSCM] ";
	private DaysManager dm = new DaysManager(this);

	@Override
	public void onEnable() {
		startPlugin();
	}

	@Override
	public void onDisable() {
		stopPlugin();
	}

	public void startPlugin() {
		if (!(configFile.exists())) {
			try {
				List<String> pvpDays = getConfig().getStringList("Days");
				pvpDays.add(null);
				List<String> pvpWorlds = getConfig().getStringList("Worlds");
				pvpWorlds.add(null);
				getConfig().createSection("Days");
				getConfig().createSection("Worlds");
				getConfig().set("Days", pvpDays);
				getConfig().set("Worlds", pvpWorlds);
				saveConfig();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		dm.checkDate();
		LogManager.logEnable();
	}

	public void stopPlugin() {
		LogManager.logDisable();
	}
}
